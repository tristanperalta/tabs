\version "2.25.4"

\header {
  title = "Holy Wars... The Punishment Due"
  composer = "Dave Mustaine"
}

guitar = {
  \key g \major
  \tempo 4 = 168

  \sectionLabel Intro
  % p6 #1
  r2 r4 r8 d16\5 dis16\5 | 
  e8\5 a,16\6 ais,16\6 b,8\6 b,16\6 ais,16\6 a,8\6 a,16\6 aes,16\6 b,8\6 a,16\6 g,16\6 |
  <e, b, e>2~-> <e, b, e>4. d16\5 dis16\5 |

  % #2
  e8\5 a,16\6 ais,16\6 b,8\6 b,16\6 ais,16\6 a,8\6 a,16\6 aes,16\6 b,8\6 a,16\6 g,16\6 |
  <fis, cis fis>2~-> <fis, cis fis>4. d16\5 dis16\5 |

  % #3
  e8\5 a,16\6 ais,16\6 b,8\6 b,16\6 ais,16\6 a,8\6 a,16\6 aes,16\6 b,8\6 e16\5 f16\5 |
  fis8\5 c16\6 cis16\6 d8\6 d16\6 cis16\6 c8\6 cis16\6 bis,16\6 b,8\6 d16\5 dis16\5 | 
  e8\5 a,16\6 ais,16\6 b,8\6 b,16\6 ais,16\6 a,8\6 a,16\6 aes,16\6 b,8\6 e,16\6 e,16\6 |

  % #4
  \repeat volta 2 {
    d16\5 (e16\5) \palmMute { e,16 e,16 e,8 } g16\5 (e16\5) \palmMute { e,16 e,16 e,16 e,16 e,16 e,16 e,16 e,16 } |

    d16\5 (e16\5) e,16 e,16 e,8 d16\5 (e16\5) e,16 e,16 e,16 e,16 e,16 e,16 <a,\6 e\5>16 <ais,\6 eis\5>16 |

    d16\5 (e16\5) e,16 e,16 e,8 g16\5 (e16\5) e,16 e,16 e,16 e,16 e,16 e,16 e,16 e,16 |

    d16\5 (e16\5) e,16 e,16 e,8 d16\5 (e16\5) e,16 e,16 e,16 e,16 e,16 e,16 <e\5 b\4>16 <ais,\6 f\5>16 |

    d16\5 (e16\5) e,16 e,16 e,8 g16\5 (e16\5) e,16 e,16 e,16 e,16 e,16 e,16 e,16 e,16 |

    d16\5 (e16\5) e,16 e,16 e,8 d16\5 (e16\5) e,16 e,16 e,16 e,16 e,16 e,16 <b,\5 fis\4>16 <g,\6 d\5>16 |
    
    \alternative {
       \volta 1 {
        d16\5 (e16\5) e,16 e,16 e,8 g16\5 (e16\5) e,16 e,16 e,16 e,16 e,16 e,16 <b,\6 fis\5>8~ |

        <b,\6 fis\5>8 e,16 e,16 e,16 e,16 <ais,\6 f\5>8~ <ais, f>8 e,16 e,16 <a,\6 e\5>8 e,8 |
        
        <b, fis>1-> |
        
        <b, fis>8-> r8 r4 \tuplet 3/2 { <b, fis>-> <g, d\5>4-> <fis,\6 cis\5>4-> } |
       }
       
       \volta 2 {
         d16\5 (e16\5) e,16 e,16 e,8 g16\5 (e16\5) e,16 e,16 e,16 e,16 e,16 e,16 b,8\6~ |
        
       }
    }
  }
  
  b,8\6 e,16 e,16 e,16 e,16 ais,8~ ais,8 e,16 e,16 a,8\6 e,8 |
  
  d16\5 (e16\5) e,16 e,16 e,8 g16\5 (e16\5) e,16 e,16 e,16 e,16 e,16 e,16 dis8\5~ |
  
  dis8\5 e,16 e,16 e,16 e,16 d8~\5 d8\5 e,16 e,16 cis8 a,8 |
 
  d16\5 (e16\5) e,16 e,16 e,8 g16\5 (e16\5) e,16 e,16 e,16 e,16 e,16 e,16 cis8\5~ |
  
  cis8\5 e,16 e,16 e,16 e,16 ais8\4~ ais8\4 \deadNote ais16\4 ais16\4 a8\4 f8\5 |
  
  % p8 #1
  \sectionLabel Interlude

  \repeat volta 2 {
    d16\5 (e16)\5 e,16 e,16 e,8 e,16 g16\5 (e16)\5 e,16 e,16 e,16 e,16 e,16 e,16 e,16 |

    d16\5 (e16\5) e,16 e,16 e,8 d16\5 (e16)\5 e,16 e,16 e,16 e,16 e,16 e,16 <b,\6 fis\5>8 |
    
    d16\5 (e16)\5 e,16 e,16 e,8 e,16 g16\5 (e16)\5 e,16 e,16 e,16 e,16 e,16 e,16 e,16 |

    d16\5 (e16\5) e,16 e,16 e,8 d16\5 (e16)\5 e,16 e,16 e,16 e,16 e,16 e,16 <b,\6 fis\5>8 |
    
    d16\5 (e16)\5 e,16 e,16 e,8 e,16 g16\5 (e16)\5 e,16 e,16 e,16 e,16 e,16 e,16 e,16 |

    d16\5 (e16\5) e,16 e,16 e,8 d16\5 (e16)\5 e,16 e,16 e,16 e,16 e,16 e,16 <b,\6 fis\5>8 |
    
    d16\5 (e16\5) e,16 e,16 e,8 d16\5 (e16)\5 e,16 e,16 e,16 e,16 e,16 e,16 <b,\6 fis\5>8 |
    
    d16\5 (e16\5) e,16 e,16 e,8 d16\5 (e16)\5 e,16 e,16 e,16 e,16 e,16 e,16 <b,\6 fis\5>8 |
  }
  
  \repeat volta 2 {
    <fis, cis>8 \glissando <g, d\5>8 e,8 <fis, cis>8 e,8 e,8 <fis, cis>8 \glissando <g, d\5>8 |
    e,8 e,8 <g, d\5>8 \glissando <fis, cis>8 e,8 e,8 <fis, cis>8 \glissando <g, d\5>8 |
    
    d16\5 (e16\5) e,16 e,16 e,8 g16\5 (e16\5) e,16 e,16 e,16 e,16 e,16 e,16 e,16 e,16 |
    
    d16\5 (e16\5) e,16 e,16 e,8 d16\5 (e16)\5 e,16 e,16 e,16 e,16 <g, b,>8 e,8 |
    
    <fis,ais,>8 \glissando <g, b,>8 e,8 <fis, ais,>8 e,8 e,8 <fis,ais,>8 \glissando <g, b,>8 |
    
    \alternative {
       \volta 1 {
         e,8 e,8 <g, b,>8 \glissando <fis,ais,>8 e,8 e,8 <fis,ais,>8 \glissando <g, b,>8 |
         d16\5 (e16)\5 e,16 e,16 e,8 e,16 g16\5 (e16)\5 e,16 e,16 e,16 e,16 e,16 e,16 e,16 |
         d16\5 (e16\5) e,16 e,16 e,8 d16\5 (e16)\5 e,16 e,16 e,16 e,16 <g, b,>8 e,8 |
       }
       \volta 2 {
         e,8 e,8 <g, b,>8 \glissando <fis,ais,>8 e,8 e,8 <fis,ais,>8 d16\5 dis16\5 |
       }
    }
  }
  
  % page 10
  \sectionLabel Verse
  \repeat volta 4 {
    e8\5 e,16 e,16 e,8 <g\5 b\4>8 \deadNote e,8 <g\5 ais\4>8 \deadNote e,8 d16\5 dis16\5 |

    e8\6 e,16 e,16 e,8 ais,8~\6 ais,8 b,8\6 a,8\6 d16\5 dis16\5 |

    e8\5 e,16 e,16 e,8 e8\5 \deadNote e,8 dis8\5 \deadNote e,8 d16\5 dis16\5 |
    e8\5 e,16 e,16 e,8 f,8 f,8 e,8 <g, b,>8 <a,\6 cis>8\5 |
    <ais,\6 d>8\5 e,8 g,8 <a, cis>8 a,8\6 e,8 <g, b,>8 <a, c>8 |
    <ais,\6 d>8\5 e,8 e,8 \glissando ais,8~\6 ais,8\6 a,8\6 g,8 a,8\6 |
    g,16 e,16 e,16 e,16 e,8 <ais,\6 d\5>8~ <ais,\6 d\5>8 <a,\6 cis\5>8 <g, ais,\5>8 a,8\6 |
    \alternative {
      \volta 1,2,3 {
        g,16 e,16 e,16 e,16 e,8 <ais,\6 f\5>8~ <ais,\6 f\5>8 <a,\6 e\5>8 <g, d\5>8 d16\5 dis16\5 |
      }
      \volta 4 {
        g,16 e,16 e,16 e,16 e,8 <ais,\6 d\5>8~ <ais,\6 d\5>8 a,8\6 g,8 c16\5 dis16\5 |
      }
    }
  }
  
  % lyrics: Holy wars ...
  e8\5 a,16\6 ais,16\6 b,8\6 b,16\6 ais,16\6 a,8\6 a,16\6 aes,16\6 b,8\6 a,16\6 g,16\6 |
  
  \time 3/4
  
  \relative { r4 r8 e'16\3 b'8.\2 a8\2 } |
  \relative { \tuplet 3/2 { b'4\2 g4\2 e'4\1 }  \tuplet 3/2 { e8 dis8 c8 } } |
  
  \time 4/4
  
  \relative { e''16\1 (c16) e16 d16 (b16) d16 c16 (a16) c16 b16 (gis16) b16 a16 (f16) a16 gis16 } |
  
  \relative { e'16\1 gis16 a16 f16 a8 b8 \glissando d8 b8 \tuplet 3/2 { a16 ais16 a16 } f8\2} |
  
  \relative { d'8\3 dis8\3 \tuplet 3/2 { e8\3 cis8\3 dis8\3 } \tuplet 3/2 { e8\3 cis8\3 dis8\3 } \tuplet 3/2 { cis8\3 c8\3 b8\3 } } |
  
  \relative { a16 c16\3 b16\3 a16\3 gis8 g8\4 fis8 f8 e4} |
  
  <e,\6 b, e b e'>1\arpeggio |
  
  % lyrics: Upon my soapbox, a leader ...
  \sectionLabel Bridge
  \tempo 4 = 122
  \relative { <c\6 g'\5>8 <b\6 fis'\5>8 e,8 <b'\6 fis'\5>8 e,8 e8 <c'\6 g'\5>8 <b\6 fis'\5>8 } |
  
  e,8 <b,\6 fis\5>8 e,8 e,8 <b,\6 fis\5>8 <c\6 g\5>8 <d\6 a\5>4 |
  
  \relative { <c\6 g'\5>8 <b\6 fis'\5>8 e,8 <b'\6 fis'\5>8 e,8 e8 <c'\6 g'\5>8 <b\6 fis'\5>8 } |
  
  e,8 <b,\6 fis\5>8 e,8 e,8 <b,\6 fis\5>8 <c\6 g\5>8 <d\6 a\5>4 \bar "||" |
  
  e,8 e,8 <e\5 b\4>8 b,8\6 fis8\5 e,8 c8\6 <c\6 g\5>8~ |
  
  <c\6 g\5>1 |
  
  e,4 e8\5  b,8\6 fis8\5 e,8 g,8 fis,8 |
  
  e'8 b8\2 fis,8 f,8 e'8 b8\2 f,8 e,8~ |
  
  e,4 e8\5  b,8\6 fis8\5 e,8 c8\6 <c\6 g\5>8~ |
  
  <c\6 g\5>1 |
  
  e,4 e8\5  b,8\6 fis8\5 e,8 g,8 fis,8 |
  
  e'8 b8\2 fis,8 f,8 e'8 b8\2 f,8 e,8~ |
  
  % lyrics: Wage the war ....
  
  e,4 e8\5  b,8\6 fis8\5 e,8 c8\6 <c\6 g\5>8~ |
  
  
  c8\6 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 |
  
  % todo: make the last 2 note a chord
  e,4 e8\5  b,8\6 fis8\5 e,8 g,8 fis,8 |
  
  % todo: make a chord
  e'8 b8\2 fis,8 f,8 e'8 b8\2 f,8 e,8~ |
  
  e,4 e8\5  b,8\6 fis8\5 e,8 c8\6 <c\6 g\5>8~ |
  
  c8\6 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 |
  
  e,4 e8\5  b,8\6 fis8\5 e,8 g,8 fis,8 |
  
  e'8 b8\2 fis,8 f,8 e'8 b8\2 <f, c\5>8 <e, b,>8~ |
  
  e,4 e8\5  b,8\6 fis8\5 e,8 c8\6 <c\6 g\5>8~ |
  
  c8\6 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 |
  
  e,4 e8\5  b,8\6 fis8\5 e,8 g,8 fis,8 |
  
  e'8 b8\2 fis,8 f,8 e'8 b8\2 f,8 e,8~ |
  
  e,4 e8\5  b,8\6 fis8\5 e,8 c8\6 <c\6 g\5>8~ |
  
  c8\6 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 <d\6 a\5>8 |
  
  e,4 e8\5  b,8\6 fis8\5 e,8 <g, d\5>8 <fis, cis>8 |
  
  e'8 b8\2 fis,8 f,8 e'8 b8\2 <f, c>8 g,8 |
  
  \sectionLabel GuitarSoloII
  
  b,8 d8 d'8 b8 g8 f8 d8 ais,8 |
  
  f8 ais8 c'8  ais8 f8 e,8 f,8 fis,8 |
  
  g,8 b,8 d'8 b8\2 g8 f8 d8 ais,8 |
  
  f8 ais8 c'8  ais8 f8 e,8 f,8 fis,8 |
  
  g,8 b,8 d'8 b8\2 g8 f8 d8 b,8 |
  
  f8 ais8 c'8  ais8 f8 e,8 f,8 fis,8 |
  
  g,8 b,8 d'8 b8\2 g8 f8 d8 b,8 |
  
  e'8 b'8 fis,8 f,8 e'8 b'8 f,8 e,8 |
}

\score {
  \new StaffGroup <<
    \new Staff { 
      \clef "treble_8"
      \omit StringNumber
      \guitar
    }
    \new TabStaff { \guitar }
  >>
  
  \midi {
    \set Staff.midiInstrument = "distorted guitar"
  }
  \layout {}
}

